/*
 * usart.c
 *
 *  Created on: Aug 13, 2019
 *      Author: tporyvaev
 */
#include "main.h"
#include <stdbool.h>
#include <string.h>
volatile bool isDMATC = true;
volatile bool isModbusOv = false;

void initUART(void)
{
	__HAL_RCC_USART2_CLK_ENABLE();
	__HAL_RCC_DMA1_CLK_ENABLE();
	huart.Instance = USART2;
	huart.Init.BaudRate = 115200;
	huart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart.Init.Mode = UART_MODE_TX_RX;
	huart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart.Init.OverSampling = UART_OVERSAMPLING_16;
	huart.Init.Parity = UART_PARITY_NONE;
	huart.Init.StopBits = UART_STOPBITS_1;
	huart.Init.WordLength = UART_WORDLENGTH_8B;
	HAL_UART_Init(&huart);
	DMA1_Channel4->CCR &= ~(1 << 0);//Disable channel 2
	hdma.Instance = DMA1_Channel4;
	hdma.Init.Direction = DMA_MEMORY_TO_PERIPH;
	hdma.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	hdma.Init.MemInc = DMA_MINC_ENABLE;
	hdma.Init.Mode = DMA_NORMAL;
	hdma.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma.Init.Priority = DMA_PRIORITY_LOW;
	HAL_DMA_Init(&hdma);

	DMA1_Channel4->CCR |= (1 << 1);//Set transfer complete interrupt
	HAL_NVIC_SetPriority(DMA1_Channel4_5_IRQn, 2, 0); //Set the lowest priority for DMA interput
	HAL_NVIC_EnableIRQ(DMA1_Channel4_5_IRQn); //Enable DMA TCIE

	//USART1->CR3 |= (1 << 0); //Enable EIE IT on ORE
	USART2->CR3 |= (1 << 7); //Enable DMA transmitet by DMAT
	//USART1->CR1 |= (1 << 26);//Enable RTOIE
	//USART1->RTOR |= 0x7FFF;//Set 20 bits timeout duration
	USART2->CR1 |= (1 << 5); //Enable RXNE IT
	HAL_NVIC_SetPriority(USART2_IRQn, 1, 0); //Set the middle priority for USART interput
	HAL_NVIC_EnableIRQ(USART2_IRQn); //Enable usart rxne and rtof it
	InitModbusTim();//Enable modbus timer
	USART2->CR1 |= (1 << 0); //Enable USART1
}
uint8_t uart_tx_b[64] = {0};
void  UART_Send(void* param)
{
	UsartSendDef *f = (UsartSendDef *)param;
	if(f->size < 64)
	{
		DMA1_Channel4->CCR &= ~(1 << 0);//Disable channel 2
		memmove(uart_tx_b, f->src, f->size);
		memset(f->src, 0 , f->size);
		DMA1_Channel4->CNDTR = (uint16_t)f->size;
		DMA1_Channel4->CMAR = (uint32_t)&uart_tx_b[0];
		DMA1_Channel4->CPAR = (uint32_t)&USART2->TDR;
		DMA1_Channel4->CCR |= (1 << 0);//Enable channel 2
	}
}
bool usbHandle = false;
uint8_t modbus_buffer[4][39] = {{0}};
uint8_t pmodbus_buffer = 0;
volatile uint8_t RxB[4][16] = {{0}};
volatile uint8_t pRxB = 0;
void modbusHandler(void* param)
{
	for (uint8_t i = 0; i < 4; i++) {
		if(RxB[i][0] == SlaveID)//Check is slave id coincidence with defined
		{
			uint16_t arr_size = FindSizeOfArray((uint8_t *)RxB[i], 16);
			uint16_t crc_RxB = ModRTU_CRC((uint8_t *)RxB[i], arr_size-2); //Calculate crc16
			uint16_t tx_crc = (RxB[i][arr_size - 1] << 8) | RxB[i][arr_size - 2]; //Find transmitted crc16
			if(crc_RxB == tx_crc)
			{
				modbus_buffer[pmodbus_buffer][0] = SlaveID;
				modbus_buffer[pmodbus_buffer][1] = RxB[i][1];
				UsartSendDef s = {0};
				switch(RxB[i][1])
				{
				case 0x01: //�����������
					if (arr_size == 8)
					{ //��������� ������
						uint16_t address = (RxB[i][arr_size - 6] << 8) | RxB[i][arr_size - 5];
						uint16_t size = (RxB[i][arr_size - 4] << 8) | RxB[i][arr_size - 3];//����������� ����
						if (size+address < 17)
						{ //����� �� 0 �� 15, ������ �� 1 �� 16
							modbus_buffer[pmodbus_buffer][2] = 2; //���-�� ���� 2
							modbus_buffer[pmodbus_buffer][4] = MainStatusRegister >> 8; //�������� �� 0 �� 7
							modbus_buffer[pmodbus_buffer][3] = MainStatusRegister; //�� 8 �� 15
							uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 5);
							modbus_buffer[pmodbus_buffer][5] = crc;
							modbus_buffer[pmodbus_buffer][6] = (crc >> 8);
							s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 7};
						}
					}
					break;
				case 0x4: //������ ������� ���������
					if(arr_size == 8)
					{
						uint16_t address = (RxB[i][arr_size - 6] << 8) | RxB[i][arr_size - 5];
						uint16_t size = (RxB[i][arr_size - 4] << 8) | RxB[i][arr_size - 3];//����������� ����
						if (size*2 + 5 < 40)
						{ //���������, �� ������������ �� ������
							if(address < 17) //����������
							{
								if(address + size < 18) //����� �� 0 �� 16, ���-�� ���� �� 1 �� 17
								{
									modbus_buffer[pmodbus_buffer][2] = size*2;
									for(uint8_t i = 0; i < size*2-1; i+=2)
									{
										if(address + i/2 < 7) //������� � ���
										{
											modbus_buffer[pmodbus_buffer][3 + i] = ModeMeanVal[address + i/2] >> 8;
											modbus_buffer[pmodbus_buffer][3 + i + 1] = ModeMeanVal[address + i/2];
										}
										if(address + i/2 == 7) //VDDA
										{
											modbus_buffer[pmodbus_buffer][3 + i] = ActualVDDA >> 8;
											modbus_buffer[pmodbus_buffer][3 + i + 1] = ActualVDDA;
										}
										if(address + i/2 > 7 && address + i/2 < 15) //�����������
										{
											modbus_buffer[pmodbus_buffer][3 + i] = TempSensVal[address-8 + i/2] >> 8;
											modbus_buffer[pmodbus_buffer][3 + i + 1] = TempSensVal[address-8 + i/2];
										}
										if(address + i/2 == 15) //RSHUNT
										{//32 ������ �����
											modbus_buffer[pmodbus_buffer][3 + i] = 0; //4 �������
											modbus_buffer[pmodbus_buffer][3 + i + 1] = GetParam(10 + SelectedShunt) >> 16; //3
											modbus_buffer[pmodbus_buffer][3 + i + 2] = GetParam(10 + SelectedShunt) >> 8; //2
											modbus_buffer[pmodbus_buffer][3 + i + 3] = GetParam(10 + SelectedShunt); //1 �������
										}
									}
									uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 3 + size*2);
									modbus_buffer[pmodbus_buffer][3 + size*2] = crc;
									modbus_buffer[pmodbus_buffer][3 + size*2 + 1] = (crc >> 8);
									s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 5 + size*2};
								}else{
									modbus_buffer[pmodbus_buffer][0] = SlaveID;
									modbus_buffer[pmodbus_buffer][1] = RxB[i][1] | (1 << 7);
									modbus_buffer[pmodbus_buffer][2] = 0x2;//address is unreachable
									uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 3);
									modbus_buffer[pmodbus_buffer][3] = crc;
									modbus_buffer[pmodbus_buffer][4] = (crc >> 8);
									s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 5};

								}
							}else{
								    modbus_buffer[pmodbus_buffer][0] = SlaveID;
									modbus_buffer[pmodbus_buffer][1] = RxB[i][1] | (1 << 7);
									modbus_buffer[pmodbus_buffer][2] = 0x2;//address is unreachable
									uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 3);
									modbus_buffer[pmodbus_buffer][3] = crc;
									modbus_buffer[pmodbus_buffer][4] = (crc >> 8);
									s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 5};
							}
						}else{ //������������ �������
							modbus_buffer[pmodbus_buffer][0] = SlaveID;
							modbus_buffer[pmodbus_buffer][1] = RxB[i][1] | (1 << 7);
							modbus_buffer[pmodbus_buffer][2] = 0x3;//requesting value is incorrect
							uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 3);
							modbus_buffer[pmodbus_buffer][3] = crc;
							modbus_buffer[pmodbus_buffer][4] = (crc >> 8);
							s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 5};

						}
					}
					break;
				case 0x06: //������ ����������� ������. 0 ����� - �������� ����
					if(arr_size == 8)
					{
						uint16_t address = (RxB[i][arr_size - 6] << 8) | RxB[i][arr_size - 5];
						uint8_t val =  RxB[i][arr_size - 3];
						if(address == 0)
						{
							SelectedShunt = val;
							modbus_buffer[pmodbus_buffer][2] = (address >> 8);
							modbus_buffer[pmodbus_buffer][3] = address;
							modbus_buffer[pmodbus_buffer][4] = (val >> 8);
							modbus_buffer[pmodbus_buffer][5] = val;
							uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 6);
							modbus_buffer[pmodbus_buffer][6] = crc;
							modbus_buffer[pmodbus_buffer][7] = (crc >> 8);
							s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 8};
						}
						if(address > 0)
						{
							modbus_buffer[pmodbus_buffer][0] = SlaveID;
							modbus_buffer[pmodbus_buffer][1] = RxB[i][1] | (1 << 7);
							modbus_buffer[pmodbus_buffer][2] = 0x2; //address is unreachable
							uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 3);
							modbus_buffer[pmodbus_buffer][3] = crc;
							modbus_buffer[pmodbus_buffer][4] = (crc >> 8);
							s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 5};

						}
					}
					break;
				case 0x05:// 0 address - ���/��� CLK ������
					if(arr_size == 8)
					{
						uint16_t address = (RxB[i][arr_size - 6] << 8) | RxB[i][arr_size - 5];
						uint16_t val = (RxB[i][arr_size - 4] << 8) | RxB[i][arr_size - 3];
						if(address == 0)
						{
							if(val == 0xff00)
							{
								usbHandle = true;
								uint8_t text[] = "ab�Control from PC";
								text[0] = 15;
								text[1] = 1;//������ �����
								text[2] = 0;//������ ������
								WriteTextToScreen(text);
								EnableMeasure();
								memmove((uint8_t *)&modbus_buffer[pmodbus_buffer][0], (uint8_t *)RxB[i], 8);
								s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 8};
							}
							if(val == 0)
							{
								usbHandle = false;
								DisableMeasure();
								memmove((uint8_t *)&modbus_buffer[pmodbus_buffer][0], (uint8_t *)RxB[i], 8);
								s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 8};
							}
							if(val !=0 && val != 0xff00)
							{
								modbus_buffer[pmodbus_buffer][0] = SlaveID;
								modbus_buffer[pmodbus_buffer][1] = RxB[i][1] | (1 << 7);
								modbus_buffer[pmodbus_buffer][2] = 0x3;
								uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 3);
								modbus_buffer[pmodbus_buffer][3] = crc;
								modbus_buffer[pmodbus_buffer][4] = (crc >> 8);
								s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 5};
							}
						}
						/*if(address == 1)
						{

						}*/
						if(address > 0)
						{
							modbus_buffer[pmodbus_buffer][0] = SlaveID;
							modbus_buffer[pmodbus_buffer][1] = RxB[i][1] | (1 << 7);
							modbus_buffer[pmodbus_buffer][2] = 0x2; //address is unreachable
							uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 3);
							modbus_buffer[pmodbus_buffer][3] = crc;
							modbus_buffer[pmodbus_buffer][4] = (crc >> 8);
							s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 5};
						}
					}
					break;
				default:
					{
					//Generate error code
					modbus_buffer[pmodbus_buffer][1] = RxB[i][1] | (1 << 7);
					modbus_buffer[pmodbus_buffer][2] = 0x01;
					uint16_t crc = ModRTU_CRC(&modbus_buffer[pmodbus_buffer][0], 3);
					modbus_buffer[pmodbus_buffer][3] = crc;
					modbus_buffer[pmodbus_buffer][4] = (crc >> 8);
					s = (UsartSendDef){&modbus_buffer[pmodbus_buffer][0], 5};
					break;
					}
				}
				Task t = {UART_Send, (void *)&s, 2, false, 1 ,(void *)&isDMATC, sizeof(s)};
				CreateTask(&t);
				if(pmodbus_buffer == 3)
				{
					pmodbus_buffer = 0;
				}else{
					pmodbus_buffer++;
				}
			}
		}
		memset((uint8_t *)RxB[i] , 0 ,16);//clear buffer
	}
}

/**
 *@brief Function calculate size of array by finding first zero entry from end
 *@param pointer of array
 *@param defined array size
 *@retval size of array
 */
uint16_t FindSizeOfArray(uint8_t *pointer, uint16_t volume)
{
	for(uint16_t i = volume; i > 0; i--)
	{
		if(*(pointer + i-1) != 0)
		{
			return i;
		}
	}
	return 0;
}
/**
 * @brief calculation of crc16 of array
 * src: https://ru.wikibooks.org/
 * @param buf pointer of array
 * @param len size of array
 * @retval crc16
 */
uint16_t ModRTU_CRC(uint8_t *buf, int len)
{
  uint16_t crc = 0xFFFF;
  for (uint8_t pos = 0; pos < len; pos++) {
    crc ^= (uint16_t)*(buf+pos);
    for (uint8_t i = 8; i != 0; i--) {
      if ((crc & 0x0001) != 0) {
        crc >>= 1;
        crc ^= 0xA001;
      }
      else
      {
        crc >>= 1;
      }
    }
  }

  // Помните, что младший и старший байты поменяны местами, используйте соответственно (или переворачивайте)
  return crc;
}
//Delay between polls shouldn't be less than 1 ms
//@brief Function inits modbus handler
TIM_HandleTypeDef htim = {0};
void InitModbusTim(void)
{
	 __HAL_RCC_TIM2_CLK_ENABLE();
	htim.Instance = TIM2;
	htim.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	htim.Init.ClockDivision = 	TIM_CLOCKDIVISION_DIV1;
	htim.Init.Prescaler = 48-1;
	htim.Init.Period = 1000-1;//20 bit duration timeout
	HAL_TIM_Base_Init(&htim);
	TIM2->DIER |= (1 << 0);//Set interrupt on update
	HAL_NVIC_SetPriority(TIM2_IRQn, 3, 0); //Set the lowest priority for TIM14 interput
	HAL_NVIC_EnableIRQ(TIM2_IRQn); //Enable TIM14 UDit

}
