/* USER CODE BEGIN Header */
/**
	MODBUS протокол:
	Slave Id - 0x11
	0x04 - Адрес - 0-6, Размер - 14 байт, кол-во 7. Возращает данные с АЦП;
		   Адрес - 7, Размер - 2 байта, кол-во 1. Возращает напряжение питания VDDA
		   Адрес - 8-14, Размер - 14 байт, кол-во 7.. Возращает температуру
		   Адрес - 15, Размер - 4 байт, кол-во 2. Возращает текущее сопротивлеие
	0x05 - Адрес - 0, Размер - 1, Включает\Выключает измерение
	0x06 - Адрес - 0, Размер - 1, Задает и устанавливает значение сопротивления шунта
	0x01 - Адрес 0, Размер - от 1 до 16, но нет смысла спрашивать меньше 16, т.к. все равно отправляется весь регистр
	Таймаут между опросами должен быть не меньше 1мс!
  */

/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stdbool.h>
#include <string.h>
ADC_HandleTypeDef hadc = {0};
DMA_HandleTypeDef hdma = {0};
TIM_HandleTypeDef htim3 = {0};
UART_HandleTypeDef huart = {0};
bool isFirstCalculate = true;
uint32_t adc_result_summ = 0;
volatile uint16_t ADC_RES[112] = {0}; //16 циклов по примерно 7 = 112

volatile uint32_t Tick = 0;
uint16_t ModeMeanVal[7] = {0}; //Максимальная скорость ~ 7Гц
uint16_t TempSensVal[7] = {0}; //Температура
/*
 * 0 бит - статус измерения (1 - ВКЛ 0 - ВЫКЛ)
 * 1 бит - статус I2C ( 0 - нормальное функционирование, 1 - была зафиксированна ошибка)
 * 2 бит - состояние TaskManager - 0 - нормальное функционирование, 1 - нет свободных ячеек для задач
 * 3 бит - 1 - электрод не подключен или измеряемое сопротивление слишком мало
 * 4 бит - 1 - ошибка в работе термометра
 * 5 бит -  1 нет слотов таймера
 */
volatile uint16_t MainStatusRegister = 0;
volatile bool isADCfull = false;
bool isMeanCalculated = false;
volatile bool isMeasurmentDisable = true;
bool isCalibrating = false;


/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC_Init(void);

/**
  * @brief  The application entry point.
  * @retval int
  */
uint32_t TickTempVal = 1000;
/**
 * @brief TM compatible blink led function
 * @param Not used
 */

int main(void)
{
 HAL_Init();
 SystemClock_Config();
 MX_GPIO_Init();
 MX_ADC_Init();
 CLKInit();
 initUART();
 initI2C();
 ModeMeanDef param = {(void *)&ADC_RES[0], sizeof(ADC_RES), ModeMeanVal, 7};
 //Task t = {ModeMean, (void *)&param, 1, false, 255, (void *)&isADCfull, sizeof(param)};
 //CreateTask(&t);
 Task t = {internal_DisablePotBetweenMeas, 0, 0, false, 255, (void *)&isADCfull, 0};
 CreateTask(&t);
 t = (Task){ModeMean, 0, 3, false, 255, (void *)&isPOTdisable, 0};
CreateTask(&t);
 t = (Task){TempCalculate, (void *)&param, 2, false, 255, (void *)&isTempMeasurment, sizeof(param)};
 CreateTask(&t);
 t = (Task){VDDACalibrating, (void *)&param, 2, false, 1, (void *)&isVDDAMeasuared, sizeof(param)};
 CreateTask(&t);
 t = (Task){modbusHandler, (void *)0, 0, false, 255 ,(void *)&isModbusOv, 0};
 CreateTask(&t);
 t = (Task){HandleButtonClick, (void *)0, 3, false, 255 ,(void *)&ButtonStatus, 0};
 CreateTask(&t);
 t = (Task){WriteMeasurmentResultToScreen, 0, 3, 0, 255, &isMeanCalculated, 0};
 CreateTask(&t);
 ConfigurateScreenDriver();
 SetIndicatorDiode(2, 1);
 SetIndicatorDiode(0, 0);
 AddTimer(150, CheckButton, true,0,0);//Отслеживаем кнопку
 ReadProfile(0);//Читаем профиль
 ScreenInit();//инициализируем дисплей
 //uint8_t test[] = {0x0, 0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8, 0x9,0xa,0xb,0xc,0xd,0xe,0xf, 0x1f};
 //EEPROMWrite(test, 17, 16);
 //WriteProfile(0);
  //SendCommandToScreen(0x28);
 TaskManager();
 while (1)
 {
	//Never runs here
 }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  //PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_SYSCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_SYSTICK_Config(48000); //Set 1ms Tickrate
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK); //Set HCLK as SysTick Source
  HAL_NVIC_SetPriority(SysTick_IRQn, 3, 0); //Set the lowest priority for Systick interput
  HAL_NVIC_EnableIRQ(SysTick_IRQn); //Enable Systick IT
}

bool IsVDDACalibrated = false;
uint16_t ActualVDDA = 3300; //Отличия текущего VDDA от 3.3В
/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{
//	HAL_ADC_Stop(&hadc);
  /* USER CODE BEGIN ADC_Init 0 */
	__HAL_RCC_ADC1_CLK_ENABLE();
  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4; //12MHz
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = ENABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START ;
  //hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING ;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  ADC1->IER |= (1 << 2);//SET EOCIE
  HAL_NVIC_SetPriority(ADC1_IRQn, 2, 0); //Set the lowest priority for ADC interput
  HAL_NVIC_EnableIRQ(ADC1_IRQn); //Enable ADC IT
/** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_VREFINT; //сначала калибируем напряжение питания
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_71CYCLES_5; //7us на измерение 142 KHz сэмплирование
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  //Make calibration
  HAL_ADCEx_Calibration_Start(&hadc);
   /* USER CODE BEGIN ADC_Init 2 */
    /* USER CODE END ADC_Init 2 */
  while(ADC1->CR & (1 << 31));
   ADC1->CR |= (1 << 0); //Set ADEN
   ADC1->CR |= (1 << 2);//Start ADC

//надо написать калибровку VDA из VREFINT
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  GPIO_InitTypeDef gtd = {0};
  //Set button pin PA1
  gtd.Mode = GPIO_MODE_INPUT;
  gtd.Pin = GPIO_PIN_1;
  gtd.Pull = GPIO_PULLUP;
  gtd.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &gtd);
  //Set PWM pin PB1
  gtd.Alternate = GPIO_AF1_TIM3;
  gtd.Mode = GPIO_MODE_AF_PP;
  gtd.Pin = GPIO_PIN_1;
  gtd.Pull = GPIO_NOPULL;
  gtd.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &gtd);
  //Set analog pin PA7
  gtd.Alternate = 0;
  gtd.Mode = GPIO_MODE_ANALOG;
  gtd.Pin = GPIO_PIN_7;
  gtd.Pull = GPIO_NOPULL;
  gtd.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &gtd);
  //PA6 в analog PIN TEMP SENS
  gtd.Alternate = 0;
  gtd.Mode = GPIO_MODE_ANALOG;
  gtd.Pin = GPIO_PIN_6;
  gtd.Pull = GPIO_PULLDOWN;
  gtd.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &gtd);
  //Set PA2 as UART TX PA3 as RX
  gtd.Alternate = GPIO_AF1_USART2;
  gtd.Mode = GPIO_MODE_AF_PP;
  gtd.Pin = GPIO_PIN_2 | GPIO_PIN_3;
  gtd.Pull = GPIO_PULLUP;
  gtd.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &gtd);
  //Set PA9 as SCL PA10 as SDA
  gtd.Pin = GPIO_PIN_9|GPIO_PIN_10;
  gtd.Mode = GPIO_MODE_AF_OD;
  gtd.Pull = GPIO_PULLUP;
  gtd.Speed = GPIO_SPEED_FREQ_HIGH;
  gtd.Alternate = GPIO_AF4_I2C1;
  HAL_GPIO_Init(GPIOA, &gtd);
}

/*
 * @brief Sets up TIM3 CH4 PWM 50% at 10KHz
 */
void CLKInit(void)
{
	 __HAL_RCC_TIM3_CLK_ENABLE();
	 TIM_OC_InitTypeDef sConfig;
	 //Sets up TIM3 to 10kHz
	 htim3.Instance = TIM3;
	 htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	 htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	 htim3.Init.Period = 100-1;
	 htim3.Init.Prescaler = 48-1;
	 //Sets up TIM3 Channel 4 as 1% Duty PWM as disabled, High on OCR compare
	 sConfig.OCFastMode = TIM_OCFAST_DISABLE;
	 sConfig.OCIdleState = TIM_OCIDLESTATE_RESET;
	 sConfig.OCMode = TIM_OCMODE_PWM1;
	 sConfig.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	 sConfig.OCNPolarity = TIM_OCNPOLARITY_HIGH;
	 sConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
	 sConfig.Pulse = 50-1;
	 HAL_TIM_PWM_MspInit(&htim3);
	 HAL_TIM_PWM_Init(&htim3);
	 HAL_TIM_PWM_ConfigChannel(&htim3, &sConfig, TIM_CHANNEL_4);
	 //Sets up OCR4REF as TRIGO3 source for ADC
	 //TIM3->CR2 |= (1 << 5)|(1 << 4)|(1 << 6); //Set TRGO at OCR4REF
	 //TIM3->CR1 |= (1 << 0);//Enable TIM3
	 //TIM3->CCER &= ~(1 << 12);//Disable TIM3 CH4
	 TIM3->DIER |= (1 << 0)|(1 << 4); //Set UIE and CC4IE (Update и CC4 прерывания)
	 HAL_NVIC_SetPriority(TIM3_IRQn, 1, 0); //Set the priority for TIM3 interput
	 HAL_NVIC_EnableIRQ(TIM3_IRQn); //Enable TIM3 IT

}


/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
