/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"
#include <stdbool.h>

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */
#define AD_Address 0x2C
#define POT_Address 0x2E
#define SCREEN_Address 0x38
#define EEPROM_Address 0x50
#define EEPROM_Page_Size 8 //������ �������� � ������
#define EEPROM_Addrees_Size 1 //������ ��������� ������ � ������
#define MaxTasksNumber 32
#define MaxTaskHeapSize 48
#define MaxTimersNumber 24
#define MaxTimersHeapSize 4
#define SlaveID 0x11
/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma;
TIM_HandleTypeDef htim3;
UART_HandleTypeDef huart;
bool IsVDDACalibrated;
uint16_t ActualVDDA;
volatile uint16_t ADC_RES[112];

volatile uint32_t Tick;
uint32_t TickTempVal;
bool MilSem;
bool isFirstCalculate;
uint32_t adc_result_summ;
uint16_t ModeMeanVal[7];
uint16_t TempSensVal[7];
uint8_t current_itterator;
volatile uint16_t MainStatusRegister;
volatile bool isADCfull;
bool isMeanCalculated;
volatile bool isMeasurmentDisable;
bool isCalibrating;
/*I2C Section*/
volatile bool isPageWritting;
volatile bool isPageWrote;
uint8_t i2c_tx[256];
uint8_t *pic2_tx;
uint8_t shval;

typedef struct{
	uint8_t *src;
	uint8_t size;
	uint8_t addr;
	uint8_t restatrt_to_read;
	uint8_t size_read;
	uint8_t *read_dest;
}I2CSendDef;
bool isRestart;
uint8_t ReadSize;
uint8_t *ReadDest;
void initI2C(void);
void I2CSend(void *p);
volatile bool isDMAI2CTC;
/*Usart Section*/


bool usbHandle;
void initUART(void);
void UART_Send(void* param);
uint16_t FindSizeOfArray(uint8_t *pointer, uint16_t volume);
void PrepareArray(void *arr, uint16_t size);
void modbusHandler(void* param);
uint16_t ModRTU_CRC(uint8_t *buf, int len);
void InitModbusTim(void);

uint8_t modbus_buffer[4][39];
uint8_t pmodbus_buffer;
volatile uint8_t RxB[4][16];
volatile uint8_t pRxB;

volatile bool isDMATC;
volatile bool isModbusOv;

typedef struct{
	uint8_t *src;
	size_t size;
}UsartSendDef;

uint8_t uart_tx_b[64];

/*TaskM Section*/

typedef struct
{
	void (*Function)(void* ); //pointer of the function
	void* param; //any types param
	uint8_t Priority; //0-4, 4 is Lowest,
	bool isSuspend; //internal var should be set as 0
	uint8_t Repetition; //Counter of repetition, 255 - continuously repetition
	void *Condition; //Condition
	uint8_t psize;//Size of pointer array, should be set as result of sizeof(param)
	uint8_t *pheap;//Pointer of param location, shouldnt be set
}Task;
Task Tasklist[MaxTasksNumber];
typedef struct
{
	uint32_t delay; //�������� � ��
	void (*target)(void*); //������� ������� ������ �����������
	bool repeat;//��������� �� ������
	uint32_t last_millis;
	uint8_t *heap; //��������� �� ����� ��������� � ������
}Timer;
//Timer TimerList[MaxTimersNumber];
void TimerManager();
int DeleteTimer(uint8_t id);
uint8_t AddTimer(uint32_t delay, void (*target)(void*), bool repeat, void* arg, uint8_t arg_size);

uint8_t CreateTask(Task *t);
void DeleteTask(uint8_t id);
void CreateTaskList(void);
void TaskManager(void);
volatile bool I2C_Free;
void RegisterWatch(void *p);
/* USER CODE END EC */
typedef struct{
	TIM_HandleTypeDef *htim;
	uint32_t channel;
	uint8_t status;
}TimerDef;

uint8_t *text_buff;
//uint8_t ptext_buff;
volatile bool NextByteTextTransaction;
volatile bool isTextTransaction;
void WriteTextToScreen(void *ptext);
void internal_text_send(void *p);
/*Math Section*/

void ModeMean(void *st);
void linear_regression(uint32_t *x, uint32_t *y, uint8_t size, int32_t *intercept_out, int32_t *slope_out);
typedef struct{
	void *arr; //pointer of source array
	uint8_t arr_size; //size of source array � ������!!
	uint16_t *target; //pointer of destination array
	uint8_t target_arr_size; //size of destination array � ������!!!!(���� 2 ����� - ���� ����� ��� uint16_t)
}ModeMeanDef;
volatile bool isTempMeasurment;
void TempCalculate(void *p);
int compare_increase(const uint16_t *i, const uint16_t *j);
int compare_decrease(const uint16_t *i, const uint16_t *j);

void EnableMeasure();
void DisableMeasure();
void SetCurrent(uint8_t code, uint8_t disable);
volatile bool isPOTdisable;
volatile bool isPOTdisabling;
void internal_DisablePotBetweenMeas(void *p);
void ConfigurateScreenDriver();
void SetIndicatorDiode(uint8_t number, uint8_t status);
void HandleButtonClick(void *p);
bool ButtonStatus;
uint8_t SelectedShunt;
void WriteMeasurmentResultToScreen(void *p);
void send_init_text(void *p);

void I2CSendTask(void *p, uint8_t priority);
volatile bool isDMARXTC;
void I2CRead(void *p);
volatile uint8_t Profile[15];
void WriteProfile(void *p);
void ReadProfile(void *p);

void internal_SendNextPage();
void EEPROMWrite(uint8_t *src, uint8_t size, uint8_t address);
volatile bool isSentReading;
volatile bool isSentRead;
void internal_ReadLastSendToEEPROM(void *p);

volatile bool isProfileReading;
volatile bool isProfileRead;
uint32_t GetParam(uint8_t number);
uint8_t Current_Code;
uint8_t ScreenRegister;
void SendCommandToScreen(void *param);
void SendCharToScreen(void *param);
uint32_t MeasurmentDelayTimer;
bool MeasurmentDelay;
volatile bool isVDDAMeasuared;
void VDDACalibrating(void *p);
volatile bool isSeriesllStart;
void StartSeries(void *p);
void CheckButton();
void ScreenInit();
/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);
void CLKInit(void);
void BlinkLed(void* t);
void TimerHandle(void* param);
/* USER CODE BEGIN EFP */
uint8_t cADC_RES;
volatile int8_t external_ittr;
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
