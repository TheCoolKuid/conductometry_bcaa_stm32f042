/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f0xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_it.h"
#include <stdbool.h>
#include <string.h>

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
//bool I2CTimer = false;
//uint32_t I2CTimerCounter = 0;
//volatile uint32_t button_counter = 0;
bool isADCShallbedisabled = false;
uint8_t cADC_RES = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/

/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M0 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}


/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
	while (1);
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVC_IRQn 0 */

  /* USER CODE END SVC_IRQn 0 */
  /* USER CODE BEGIN SVC_IRQn 1 */

  /* USER CODE END SVC_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}
/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void) //������������� ~ 2.3�� ��������
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  Tick++;
  /*if(Tick > button_counter) //������ �������� ������
  {
	  button_counter+=250;
	  if(!(GPIOA->IDR & (1 << 1)) && !ButtonStatus)
	 	  {
	 		  ButtonStatus = true;
	 	  }
  }*/
 /* if(MeasurmentDelay && MeasurmentDelayTimer+100 < Tick && isMesurmentDisable) //������������ �������� � �������� 100�� ����� �����������
  {
	  MeasurmentDelay = false;
	  isMesurmentDisable = false;
	  cADC_RES = 0;
	  external_ittr = -1;
	  isSeriesllStart = true;
	  //TIM3->CR1 |= (1 << 0);//Enable TIM3
	  //TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
	  //I2CTimer = false;
  }*/
  /*if(MeasurmentDelay && MeasurmentDelayTimer+50 == Tick && isMesurmentDisable) //������������ ������ � +50 �� �� ����� ��������� ��� ��������� �����������
  {
  	  //MeasurmentDelay = false;
  	  //isMesurmentDisable = false;
	  cADC_RES = 0;
  	 external_ittr = -1;
  	 ADC1->CHSELR |= (1 << 6);//�������� ����� 6 TEMP_SENS
  	 ADC1->CHSELR &= ~(1 << 7);//��������� ����� 7 ANALOG
  	 ADC1->CR |= (1 << 2); //Start ADC conversation
  	  //SetCurrent(30, 2); //�������� ���������
  	  //TIM3->CR1 |= (1 << 0);//Enable TIM3
  	  //TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
  	  //I2CTimer = false;
   }*/
  /*if(I2CTimer &&  I2CTimerCounter+1 < Tick) //�������� ~ 5ms, ����� ������������ ���������� ������������
  {
	  if(!isMeasurmentDisable)
	  {
		  I2CTimer = false;
		  TIM3->CR1 |= (1 << 0);//Enable TIM3
		  TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
	  }
  }*/
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

volatile int8_t external_ittr = -1;
bool disable_clk = false;
uint8_t CycleNum = 0;

void ADC1_IRQHandler(void)
{
	if(ADC1->ISR & (1 << 2)) //check is it EOC IT
	{
		ADC_RES[cADC_RES] = ADC1->DR;
		if(cADC_RES < 111)
		{
			cADC_RES++;
		}else{
			cADC_RES = 0;
		}
		if(external_ittr == 15 && cADC_RES > 95)//��������� �����
		{
			external_ittr = -1;
			cADC_RES = 0;
			ADC1->CR |= (1 << 4); //Disable ADC
			if(ADC1->CHSELR & (1 << 7))//���� 1, �� ���������� �����������, ������� ������ ��������� CLK ���, �� ���� ���
			{
				//disable_clk = true;//��������� ������ ����� ��� LOW � Update IT (��� ��� ����� 0), ��-�� ����� ���������� 17 ���������
				isADCShallbedisabled = true;
				isADCfull = true; //������ �����
			}
			if(ADC1->CHSELR & (1 << 6))
			{
				isTempMeasurment = true;
			}
			if(ADC1->CHSELR & (1 << 17))
			{
				isVDDAMeasuared = true;
			}
		}
		if((ADC1->CHSELR & (1 << 6) && external_ittr < 15) || (ADC1->CHSELR & (1 << 17) && external_ittr < 15))
		{
			external_ittr++; //����������� ��������� �� ����� ��������� ����������� � ����
		}
	}

}
void DMA1_Channel4_5_IRQHandler(void)
{
	if(DMA1->ISR & (1 << 13))//Check is it TCIF4
	{
		DMA1->IFCR |= (1 << 13);//Clear TCIF4 flag
		DMA1_Channel4->CCR &= ~(1 << 0);//Disable channel
		memset(uart_tx_b, 0, 64);//cleart tx buffer
		isDMATC = true;
	}
}
volatile uint8_t *puart_rx = RxB[0];
void USART2_IRQHandler(void)
{
	if(USART2->ISR & (1 << 5)) //Check is RXNE set
	{
		if(!(TIM2->CR1 & (1 << 0)))//if TIM2 enable
		{
			TIM2->CR1 &= ~(1 << 0);//Disable TIM2
		}
		*puart_rx = USART2->RDR;
		if(puart_rx == &RxB[pRxB][15])
		{
			puart_rx = &RxB[pRxB][0];
		}else{
			puart_rx++;
		}
		TIM2->CR1 |= (1 << 0); //Enable TIM2
	}
	if(USART2->ISR & (1 << 3) && !(USART2->ISR & (1 << 5)))//Check is ORE set and RXNE reset
	{
		USART2->ICR |= (1 << 3); //Clear ORE flag
	}
	/**if(USART1->ISR & (1 << 11))//Check is RTOF set
	{
		USART1->ICR |= (1 << 11);//Clear RTOF
		USART1->CR2 &= ~(1 << 23);//Disable modbus idle timer
		if(pRxB == 3)
		{
			pRxB = 0;
		}else{
			pRxB++;
		}
		puart_rx = &RxB[pRxB][0];
		if(isModbusOv == false)
		{
			isModbusOv = true;
		}
	}*/
}

void I2C1_IRQHandler(void)
{
	/*if(I2C1->ISR & (1 << 1))//Check i TXIS set
	{
		I2C1->TXDR = *pic2_tx;
		if(pic2_tx == &i2c_tx[15])
		{
			pic2_tx = &i2c_tx[0];
		}else{
			pic2_tx++;
		}
	}*/
	if(I2C1->ISR & (1 << 8) || I2C1->ISR & (1 << 9) || I2C1->ISR & (1 << 10) || I2C1->ISR & (1 << 12))
	//Check BERR, OVR, ARLO, TIMEOUT bits and reload I2C
	{
		MainStatusRegister |= ( 1 << 1 );//������������� ������
		DMA1_Channel2->CCR &= ~(1 << 0);//Disable channel2 I2C_TX
		memset(i2c_tx, 0, 16);//cleart tx buffer
		isDMAI2CTC = true; //������������� ��� ���������� �����������
		//I2CTimer = true; //����, ��� ����� �������� ������ (����� �� �������� ������ ���������)
		//memcpy(&current_delay_timer, &Tick, 4);
		I2C1->CR1 &= ~(1 << 0); //Set PE=0
		for(uint8_t i = 0; i < 10; i++); //���� ����� 10 ������ APB ����
		while(I2C1->CR1 & (1 << 0)); //���� ���� ������ ��������� ����������
		I2C1->CR1 |= (1 << 0); //Set PE=1
	}
	if(I2C1->ISR & (1 << 6))//Check TC
	{
			isDMAI2CTC = false;
			isDMARXTC = false;
			I2C1->CR2 |= (ReadSize << 16)|(1 << 25)|(1 << 10)|(1 << 13);//set slave addr, nbytes, autoend
	}
	if(I2C1->ISR & (1 << 4))//Check NACKF
	{
		I2C1->ICR |= (1 << 4);//������� NACK
		I2C1->CR2 |= (1 << 14); //���������� ����
	}
	if(I2C1->ISR & (1 << 5))//Check STOPF
	{
		I2C1->ICR |= (1 << 5);//������� STOPF
		I2C_Free = true; //���������� ��� ���� ��������
		if(!isMeasurmentDisable && (((I2C1->CR2 & 0xFF) >> 1) == POT_Address) && !isPOTdisabling)
		{
			//CurrentTransaction = false;
			cADC_RES = 0;
			external_ittr = -1;
		    isADCShallbedisabled = false;
			TIM3->CR1 |= (1 << 0);//Enable TIM3
		    TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
						//I2CTimer = true; //������ ���� �������� ���������� �������
						//memcpy(&I2CTimerCounter, (void *)&Tick, 4); //�������� ��������
						//TIM3->CR1 |= (1 << 0);//Enable TIM3
						//TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
		}
		if(isTextTransaction && (((I2C1->CR2 & 0xFF) >> 1) == SCREEN_Address))
		{
			NextByteTextTransaction = true; //���������� �����
		}
		if(isSentReading && (((I2C1->CR2 & 0xFF) >> 1) == EEPROM_Address))
		{
			isSentRead = true; //����� ��������� ��������
		}
		if(isProfileReading && (((I2C1->CR2 & 0xFF) >> 1) == EEPROM_Address))
		{
			isProfileRead = true; //������ ���� ��� ������� ��������
			isProfileReading = false;
		}
		if(isPOTdisabling && (((I2C1->CR2 & 0xFF) >> 1) == POT_Address))
		{
			isPOTdisabling = false;
			isPOTdisable = true;
			disable_clk = true;
		}
	}
}

void DMA1_Channel2_3_IRQHandler()
{
	if(DMA1->ISR & (1 << 5))//Check is it TCIF2 I2C TX TC
		{
			DMA1->IFCR |= (1 << 5);//Clear TCIF5 flag
			DMA1_Channel2->CCR &= ~(1 << 0);//Disable channel
			//memset(i2c_tx, 0, 16);//cleart tx buffer
			isDMAI2CTC = true; //����, ��� ���������� �����������
			isDMARXTC = true;
			//I2CTimer = true; //����, ��� ����� �������� ������
			//current_delay_timer = Tick;
			//memcpy(&current_delay_timer, &Tick, 4);
			/*if(!isMeasurmentDisable && (((I2C1->CR2 & 0xFF) >> 1) == POT_Address) && !isPOTdisabling)
			{
				//CurrentTransaction = false;
				cADC_RES = 0;
				external_ittr = -1;
				isADCShallbedisabled = false;
				TIM3->CR1 |= (1 << 0);//Enable TIM3
				TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
				//I2CTimer = true; //������ ���� �������� ���������� �������
				//memcpy(&I2CTimerCounter, (void *)&Tick, 4); //�������� ��������
				//TIM3->CR1 |= (1 << 0);//Enable TIM3
				//TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
			}
			if(isTextTransaction && (((I2C1->CR2 & 0xFF) >> 1) == SCREEN_Address))
			{
				NextByteTextTransaction = true; //���������� �����
			}
			if(isPageWritting && (((I2C1->CR2 & 0xFF) >> 1) == EEPROM_Address))
			{
				isPageWrote = true; //����� ��������� ��������
			}
			if(isPOTdisabling && (((I2C1->CR2 & 0xFF) >> 1) == POT_Address))
			{
				isPOTdisabling = false;
				isPOTdisable = true;
				disable_clk = true;
			}
			//if(!isMesurmentDisable)
			//{
				//TIM3->CCER |= (1 << 12);//�������� CLK
			//}*/
		}
	if(DMA1->ISR & (1 << 9)) //I2X RX TC
	{
		DMA1->IFCR |= (1 << 9);//Clear TCIF5 flag
		DMA1_Channel3->CCR &= ~(1 << 0);//Disable channel
		isDMARXTC = true;
		isDMAI2CTC = true;
	}
}
void TIM2_IRQHandler(void)
{
	if(TIM2->SR & (1 << 0))//Check is it UIF
	{
		TIM2->SR &= ~(1 << 0);//Clear UIF
		TIM2->CR1 &= ~(1 << 0);//Disable TIM14
		if(pRxB == 3)
		{
			pRxB = 0;
		}else{
			pRxB++;
		}
		puart_rx = &RxB[pRxB][0];
		if(isModbusOv == false)
		{
			isModbusOv = true;
		}
	}
}
//������ PWM ���� �� Update �� CC (������� � ������� ���, ���������� 93 ��������� �� 16 �������� => ����� �������� 33���)
void TIM3_IRQHandler(void)
{
	if(TIM3->SR & (1 << 0)) //Update Event
	{
		TIM3->SR &= ~1;//CLEAR flag
		if (!isADCShallbedisabled) {
			ADC1->CR |= (1 << 2); //Start ADC conversation
			if(external_ittr < 15)
			{
				external_ittr++;
			}
		}
	}
	if(TIM3->SR & (1 << 4)) //Compare Event
	{
			TIM3->SR &= ~(1 << 4);//Clear Flag
			if (!isADCShallbedisabled) {
				ADC1->CR |= (1 << 4); //Stop ADC conversation
			}
			if(disable_clk)
			{
				disable_clk = false;
				//isADCfull = true; //������ �����
				//CycleCounter = 0;
				TIM3->CCER &= ~(1 << 12);//Disable TIM3 CH4
				TIM3->CR1 &= ~(1 << 0);//Disable TIM3
			}

	}
}
/******************************************************************************/
/* STM32F0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f0xx.s).                    */
/******************************************************************************/

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
