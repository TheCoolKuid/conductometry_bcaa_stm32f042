/*
 * Math.c
 *
 *  Created on: 11 авг. 2019 г.
 *      Author: tporyvaev
 */
#include "main.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
void MeasureDelay();
void MeasureTempDelay();
/**
 * @brief Function calculate a mode mean of some uint16_t array.
 * @param ModeMeanDef struct define arr pointer, arr size(not more than 64), target of calculated val
 * @retval Write into ModeMeanDef->target
 * ����������� �������� �������� ��������� ���� ������� ������� ADC_CODE = f(current_code)
 * � ������ ����������� ��� ������������� �������� �� 20
 * ����� �������� �������� �������������: RESULT * (VDDA/(2^13-1)) /(VREF4*10/(RSHUNT*258))
 * ����������� ������������� - 500��
 *
 * �������� ���������:
 * ������ ��� ��� Update �� TIM3 CLK ��������� � HIGH ���������
 * � ���� ������ ����������� continuously(�����������) ��������� ������ 5.67us => �� 34us
 * ~6 ���������, ����� �� 4 ���������, ��� ���� ~2 ������ ��������� �������� �� ����������
 * ������� (������ ����� CLK � �������� �������� ���� ��������) => �.�. ����� ���������
 * �� ������ ��� ��������� ���������� ���������� ����� => �������������� ��������� ��������
 * � ��� 4 ��������� ���������� ����� ������������ �������
 * ����� ����� 4 ���������?
 * ������ �� ������� f13 = f12*4 - ����� ��������� ���������� �� 1 ��� ���������� ��������� �������
 * ������������� � 4 ���� => ������ 10���, ������ ����� ������������ �� ��������� 40���, �.�.
 * ��� �� �������� �������� ����� CLK � LOW, �� �� ������ HIGH �� �������� 4 ��������� (�����������
 * ������� 177 ���) => �������� � 4 ���� ������ �������. ���������� ��������� - ����� ����� ���� ��������
 * �� 1 ��� ������ (����� �� 2) (���������) � ���������� ����� ����� �� ����������� ���-�� �������.
 * ����� �������, � ������ ����, ��� ��������� ���������� �� 10(����������� 1 ����� ����� �������), ���������
 * ���������� - 1���, � ����������������� - 500�� (� ��� �� ������������, � �������� ����������)
 *
 * ����� �������� ��� ���� ��� � �������� ���������� � 14 ���, �� ���� ��� 55.5 ������ �� ���������
 * ������ ������, � ��� ������� ��� ������ ������
 */
uint8_t ptarget = 0;
uint32_t temp_avg_value[7] = {0};
uint32_t current_value[] = {39, 54, 69, 84, 99, 114, 129};
uint8_t current_itterator = 0; //������� ����
void ModeMean(void* st)
{
	if (!isMeasurmentDisable) {
		uint32_t TempAverageValue = 0;
		PrepareArray((void*)&ADC_RES[0], 112);
		for(int i = 0; i < 64; i++) //����� ������ 64 ������ ��� ��������
		{
			TempAverageValue = TempAverageValue+ADC_RES[i]*100/22;
		}
		uint32_t avg = TempAverageValue/32; //��������� (�������� ��������� - ����� �� 2 (����� �� 1 ���), ��������� �� 16)
		if(TempAverageValue % 32 >= 16) //��������� �� �������������
		{
			avg++;
		}
		temp_avg_value[current_itterator] = avg;
		if(current_itterator == 6)
		{
			//��� ����� ������ �������������
			int32_t slope = 0;
			int32_t inter = 0;
			linear_regression(&current_value[0], &temp_avg_value[0], 7, &inter, &slope);
			//��������� ����� �������� �������������� �������������� �, �������������, ������ �� ������
			//�� � �� ��������� ���������� ������� - 1000 �� (1:100 �� �����), ���, �� ����� ������ �����
			//������� ����� ������� ��� ���������� ������� �������� �������� �� ���������� � 500�� (1:200 �� �����)
			ModeMeanVal[ptarget] = slope;
			if(ptarget < 6)
			{
				ptarget++;
			}else{
				ptarget = 0;
				if (!usbHandle) {
					isMeanCalculated = true;
				}
			}
			//isMesurmentDisable = true;
			//memcpy(&MeasurmentDelayTimer, &Tick,4);
			//if(slope == 0)
			//{
			//	//DisableMeasure(); //��������� ��������� ���� �������� �� ���������
				//MeasurmentDelay = false;
				//MainStatusRegister |= (1 << 3); //���� ��� ��������� ���
			//}else{
				MeasurmentDelay = true;
				//Task t = {StartSeries, (void *)0, 3, false, 1 ,(void *)&isSeriesllStart, 0};
				AddTimer(100, MeasureDelay, false,0,0);
				AddTimer(50, MeasureTempDelay, false,0,0);
				//CreateTask(&t);
			//}
			//current_itterator = 0;
		}
		//���������� ����� �������� ����
		if(current_itterator < 6)
		{
			current_itterator++;
		}else{
			current_itterator = 0;
		}
		memset((void *)ADC_RES, 0, 112*2);
		//uint8_t send[2] = {0};
		//send[0] = 0x20; //��������� ��������
		//send[1] = 30 + current_itterator*30; //���������� ���
		//I2CSendDef f = { send, 2, AD_Address}; //����
		//Task t = {I2CSend, (void *)&f, 1, false, 1, &isDMAI2CTC, sizeof(f)}; //������ ���� �� �������� ������
		//CreateTask(&t);
		if (!MeasurmentDelay) {
			SetCurrent(39 + current_itterator*15, 0); //���������� ���, ��������� ������������
		}else{
			SetCurrent(0, 0); //���������� ���, ��������� ������������
		}
		if(MeasurmentDelay)
		{
			isMeasurmentDisable = true;
		}
		//Task t = {(void *)internal_DisablePotBetweenMeas, 0, 1, false, 1, (void *)&isADCfull, 0};
		//CreateTask(&t);
		//I2CSend((void *)&f);

		//if(!isMesurmentDisable)
		//{
			//TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
		//}
	}

}
//����������� ����������� ������� y = slope*x + intercept
// x,y - ��������� ������� �������� � � y
//size - ������ ��������
//intercept_out, slope_out - ��������� ���������� ������
// ax^2 + bx = xy
// ax + bn = y
//������� �������
// x^2  x
// x  n
//������� �
//�� �
//� n
//������� �
//�^2 ��
//� �
void linear_regression(uint32_t *x, uint32_t *y, uint8_t size, int32_t *intercept_out, int32_t *slope_out)
{
	uint32_t x2summ = 0;
	uint32_t xsumm = 0;
	uint32_t ysumm = 0;
	uint32_t xysumm = 0;
	int32_t det_main = 0;
	int32_t det_slope = 0;
	int32_t det_intercept = 0;
	for(uint8_t i = 0; i < size; i++) //������� ����������� ������� ���������
	{
		x2summ+=(*(x+i))*(*(x+i));
		xsumm+=*(x + i);
		ysumm+=*(y + i);
		xysumm+=(*(x+i)) * (*(y + i));
	}//������� ������������ ������������ ��� det_main!=0
	det_main = x2summ*size - xsumm*xsumm;
	det_slope = xysumm*size - xsumm*ysumm;
	det_intercept = x2summ*ysumm - xysumm*xsumm;
	if(det_main == 0)
	{
		det_main = 1; //�� ������ ������ ��������� ������������ �� ����
	}
	uint32_t intr = det_intercept/det_main;//������� �����
	if(det_intercept%det_main >= det_main/2)
	{
		intr++;//���������
	}
	uint32_t slp = det_slope/det_main; //������� ������
	if(det_slope%det_main >= det_main/2)
	{
		slp++;//���������
	}
	*intercept_out = intr;
	*slope_out = slp;
}

volatile bool isTempMeasurment = false;
uint8_t temp_incr = 0;
/**
 * ����������� ����������� �� ����������� ����������
 * ��������� �� ��������� ������� -36.3 - 363
 */
void TempCalculate(void *p)
{
	ADC1->CHSELR |= (1 <<7);//�������� ����� 6 TEMP_ANALOG
 	ADC1->CHSELR &= ~(1 << 6);//��������� ����� 6 TEMP_SENS
 	uint16_t temp_intercept = 500;
 	uint16_t temp_slope = 10;
 	temp_intercept = (uint16_t)GetParam(2);
 	temp_slope = (uint8_t)GetParam(3);
 	uint32_t TempAverageValue = 0;
 	PrepareArray((void*)&ADC_RES[0], 112);
 	for(int i = 0; i < 64; i++)
 	{
 		TempAverageValue += ADC_RES[i]/2;
 	}
 	uint32_t avg = TempAverageValue/32; //��������� (�������� ��������� - ����� �� 2 (����� �� 1 ���), ��������� �� 16)
 	if(TempAverageValue % 32 >= 16) //��������� �� �������������
 	{
 		avg++;
 	}
 	//uint16_t temp =
 	uint32_t y_vtg = avg*ActualVDDA;
 	y_vtg = y_vtg/819;
 	if(y_vtg !=0)
 	{
 		TempSensVal[temp_incr] = (y_vtg - temp_intercept*10)/temp_slope; // avg = intrcpt + slope*T
 	}else{
 		MainStatusRegister |= (1 << 4); //���������� ������ ����������
 	}
 	//if(TempSensVal[temp_incr] > 0 && TempSensVal[temp_incr] < 90)
 	//{
 		//NVIC_SystemReset(); //�������
 	//}
 	if(temp_incr < 6) //��������� �����
 	{
 		temp_incr++;
 	}else{
 		temp_incr = 0;
 	}
}
volatile bool isVDDAMeasuared = false;
void VDDACalibrating(void *p)
{
	IsVDDACalibrated = true; //������ ����
	ADC1->CHSELR |= (1 << 7);//�������� ����� 7
	ADC1->CHSELR &= ~(1 << 17);//��������� ����� VREFINTs
	ADC1->CHSELR &= ~(1 << 6);//�������� ����� 6 TEMP_SENS
	uint32_t TempAverageValue = 0;
	PrepareArray((void*)&ADC_RES[0], 112);
	for(int i = 0; i < 64; i++)
	{
		TempAverageValue += ADC_RES[i]*10;
	}
 	uint32_t avg = TempAverageValue/32; //��������� (�������� ��������� - ����� �� 2 (����� �� 1 ���), ��������� �� 16)
 	if(TempAverageValue % 32 >= 16) //��������� �� �������������
 	{
 		avg++;
 	}
	uint16_t calibrated_value = *((uint16_t*)0x1FFFF7BA);//������������� �������� � ������
 	uint32_t ActualVDDAt = 66000*calibrated_value; //��������� ����������� (�.�. ��������� 13������, ����� ��������� ������������� � 13������ ���)
 	ActualVDDA = ActualVDDAt / avg;
}

int compare_increase(const uint16_t *i, const uint16_t *j)
{
	return *i - *j;
}

int compare_decrease(const uint16_t *i, const uint16_t *j)
{
	return *j - *i;
}
/*@brief �������� ������ �� �������� � �������� � �������� ����
 * @param arr - ��������� �������
 * @param size - ������ �������
 */
void PrepareArray(void *arrr, uint16_t size)
{
	uint16_t *arr = arrr;
	qsort(arr, size, sizeof(uint16_t), (int(*) (const void *, const void *))compare_increase);
	uint32_t mediane = 0;
	if(size % 2 != 0) //�������� �� ��������
	{
		mediane = *(arr + size/2+1); //����� �������
	}else{
		mediane = (*(arr +size/2) + *(arr + size/2 - 1))/2; //����� ��������� ������ �������� size/2 - 1 + ������ ������ size/2
	}
	uint32_t Qlow = (ADC_RES[size/4] + ADC_RES[size/4+1])/2;//������ �������� - ����� ������ ��������, ������ ������, ������ ������
	uint32_t Qhigh = (ADC_RES[3*size/4] + ADC_RES[3*size/4 + 1])/2;//�������
	uint16_t Qbetween = Qhigh - Qlow;//��������
	for(uint8_t i = 0; i < size; i++)
	{
		if(*(arr + i) < (mediane - Qbetween) || *(arr + i) > (mediane + Qbetween))
		{
			*(arr + i) = 0; //������� �������������� �������
		}
	}
	qsort(arr, size, sizeof(uint16_t), (int(*) (const void *, const void *))compare_decrease);
}

void MeasureDelay()
{
	 if(MeasurmentDelay && isMeasurmentDisable) //������������ �������� � �������� 100�� ����� �����������
	  {
		  MeasurmentDelay = false;
		  isMeasurmentDisable = false;
		  cADC_RES = 0;
		  external_ittr = -1;
		  SetCurrent(39, 0); //�������� ���������
		  //TIM3->CR1 |= (1 << 0);//Enable TIM3
		  //TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
	  }

}
void MeasureTempDelay()
{
	  if(MeasurmentDelay && isMeasurmentDisable) //������������ ������ � +50 �� �� ����� ��������� ��� ��������� �����������
	  {
	  	  //MeasurmentDelay = false;
	  	  //isMesurmentDisable = false;
		  cADC_RES = 0;
	  	 external_ittr = -1;
	  	 ADC1->CHSELR |= (1 << 6);//�������� ����� 6 TEMP_SENS
	  	 ADC1->CHSELR &= ~(1 << 7);//��������� ����� 7 ANALOG
	  	 ADC1->CR |= (1 << 2); //Start ADC conversation
	  	  //SetCurrent(30, 2); //�������� ���������
	  	  //TIM3->CR1 |= (1 << 0);//Enable TIM3
	  	  //TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
	  	  //I2CTimer = false;
	   }
}
