/*
 * measurement.c
 *
 *  Created on: 26 ����. 2019 �.
 *      Author: tporyvaev
 */

#include "main.h"
#include "string.h"
///@brief �������� ���������
void EnableMeasure()
{
	isMeasurmentDisable = false;
	MainStatusRegister |= ( 1 << 0 );
	//TIM3->CR1 |= (1 << 0);//Enable TIM3
	//TIM3->CCER |= (1 << 12);//Enable TIM3 CH4
	//GPIOA->ODR &= ~GPIO_PIN_4; //Enable LED
	current_itterator = 0; //������� ���������
	//ConfigurateScreenDriver();
	SetIndicatorDiode(1, 0); //�������� ���
	ReadProfile(0);
	SetCurrent(39, 0); //�������� ���������
	//I2CSend((void *)&f);
}
///@brief ��������� ���������
void DisableMeasure()
{
	MainStatusRegister &= ~( 1 << 0 );
	TIM3->CR1 &= ~(1 << 0);//Disable TIM3
	TIM3->CCER &= ~(1 << 12);//Disable TIM3 CH4
	//GPIOA->ODR |= GPIO_PIN_4; //Disabe LED
	//memset(ModeMeanVal, 0, 7); //������ ������� � ����������
	current_itterator = 0; //������� ���������
	MeasurmentDelay = false; //������� ��������
	SetCurrent(0, 0);//������ ��� 30, ��������� ������ � ������
	isMeasurmentDisable = true;
	send_init_text(0);
	SetIndicatorDiode(1, 1); //��������� ���
}
uint8_t Current_Code = 0;
bool MeasurmentDelay = false;
uint32_t MeasurmentDelayTimer = 0;
/*
 * @brief ������������� ��� � ������ ������� �� ������������ ������
 * @param code �������� ����
 * @param disable ��������/��������� ������������
 */
void SetCurrent(uint8_t code, uint8_t disable)
{
	Current_Code = code;
		switch(disable)
		{
		case 0: //������������� ��� �� ��������� ���������
			{
				uint8_t send[] = { 0x0, code}; //���������� ��� � ������� �������������
				I2CSendDef f = { send, 2, POT_Address,0};
				I2CSendTask((void *)&f, 0);
			}
			break;
			/**
			 * � ���� ������ �� ������������
			 * ������ ������� ��������� �� ������� ������ � ����, ��������, � �� ���� ������
			 */
		/*case 1: //������������� ��� � ���������
			{
				uint8_t send[] = { 0x0, code, 0x40, 0xb0}; //���������� ��� � ������� �������������, ��������� �������� � � TCON
				I2CSendDef f = { send, 4, POT_Address,0};
				I2CSendTask((void *)&f, 1);
			}
			break;
		case 2://������������� ��� � ��������
		{
			uint8_t send[] = { 0x0, code, 0x40, 0xf0}; //�������� �������� �0
			I2CSendDef f = { send, 4, POT_Address,0};
			I2CSendTask((void *)&f, 1);
		}
		case 3: //���������
		{
			uint8_t send[] = {0x40, 0xb0}; //��������� �������� � � TCON
			I2CSendDef f = { send, 2, POT_Address};
			I2CSendTask((void *)&f, 1);
		}
		case 4: //��������
		{
			uint8_t send[] = {0x40, 0xf0}; //�������� �������� � � TCON
			I2CSendDef f = { send, 2, POT_Address};
			I2CSendTask((void *)&f, 1);
		}
			break;*/
		}
}
//������������� ��������� ������� �� ������
void ConfigurateScreenDriver()
{
	uint8_t send[] = { 0x3, 0x0}; //� ������� ������������ ���������� ��� 4 � 5 -������
	I2CSendDef f = { send, 2, SCREEN_Address,0};
	//I2CSend((void *)&f);
	I2CSendTask((void *)&f, 3);
}
uint8_t ScreenRegister = 0;
/*
 * ������� ������ ���������
 * ��� - 1
 * ��� - 0
 * ��� - 2
 */
void SetIndicatorDiode(uint8_t number, uint8_t status)
{
	switch(number)
	{
	case 0: //���
		if(status)
		{
			ScreenRegister |= ( 1<< 4);
		}else{
			ScreenRegister &= ~( 1<< 4);
		}
		break;
	case 1: //���
		if(status)
		{
			ScreenRegister |= ( 1 << 5);
		}else{
			ScreenRegister &= ~( 1 << 5);
		}
		break;
	case 2://���
		if(status)
		{
			ScreenRegister |= ( 1 << 5)|(1 << 4);
		}else{
			ScreenRegister &= ~(( 1 << 5)|(1 << 4));
		}
		break;
	default:
		ScreenRegister &= ~(( 1 << 5)|(1 << 4));
		break;
	}
	uint8_t send[] = { 0x01, ScreenRegister}; //�������� enable �� 5 �� SD, ������ ���
	I2CSendDef f = { send, 2, SCREEN_Address};
	//I2CSend((void *)&f);
	I2CSendTask((void *)&f, 3);
}

bool ButtonStatus = false;
void HandleButtonClick(void *p)
{
	if(MainStatusRegister & (1 << 0)) //��������� ���
	{
		DisableMeasure();
	}else{ //����
		EnableMeasure();
	}
}

volatile bool isPOTdisable = false;
volatile bool isPOTdisabling = false;
void internal_DisablePotBetweenMeas(void *p)
{
	SetCurrent(0,0);//��������� ������������
	//while(!I2C_Free);
	//uint8_t send[] = { 0x0, 0}; //���������� ��� � ������� �������������
	//I2CSendDef f = { send, 2, POT_Address,0};
	//I2CSend(&f);
	isPOTdisabling = true;
	//AddTimer(5, ModeMean, 0,0,0);
}
uint8_t button_press_counter = 0;
bool isButtonStillPressed = false;
void CheckButton()
{
	if(!(GPIOA->IDR & (1 << 1)))
	{
		 button_press_counter++;
		 if(button_press_counter == 255)
		 {
			 button_press_counter = 0;
		 }
	}else{
		if(button_press_counter >= 1 && button_press_counter < 5) //���� �� ������ ���� - �� ���������� 250�� ������ ������ �� ������ => ���������/����������
		{
			ButtonStatus = true;
			button_press_counter = 0;
		}
		if(button_press_counter >= 8 && button_press_counter < 21) //������ �� 2 �� 5 �
		{
			if(!(MainStatusRegister & (1 << 0))) //���������, �� �������� �� ���������
			{
				uint32_t new_shunt = 0;
				if(SelectedShunt == 4) //�������� �������� ���������� �����
				{
					new_shunt = GetParam(11);
				}else{
					new_shunt = GetParam(10 + SelectedShunt + 1);
				}
				if(new_shunt != 0) //���� ��� �� �������, �� ����������� �� ����
				{
					if(SelectedShunt < 4)
					{
						SelectedShunt++;
					}else{
						SelectedShunt = 1;
					}
				}else{
					SelectedShunt = 1; //���� ��������� ���� �������, �� ����������� �� ������
				}
				 send_init_text(0);
			}
			button_press_counter = 0;
		}
	}
}

void send_init_text(void *p)
{
	/*uint8_t text[] = "ab�Ready to workbPress to start";
	text[0] = 28; //������
	text[1] = 1; //������� �� �����
	text[2] = 0; //����� ������
	text[16] = 0x0A; //������� �������*/
	char text[36] = {0};
	uint32_t elecrt = 0;
	elecrt = GetParam(10 + SelectedShunt);
	sprintf(&text[3], "Ready to Work\n%uKOm electrode", (uint16_t)(elecrt/1000));
	text[1] = 1;
	text[2] = 0;
	text[0] = FindSizeOfArray((uint8_t *)text, 36) - 3;
	WriteTextToScreen(text);
}

void ScreenInit()
{
	uint8_t parm1[] = {0x30,0};
	uint8_t parm2[] = {0x20,0}; //������������� 4 bit �����
	uint8_t parm3[] = {0x28,1}; //������������� 2 ������
	uint8_t parm4[] = {0x8,1}; //��������� �����
	uint8_t parm5[] = {0x1,1}; //������� �������
	uint8_t parm6[] = {0x6,1}; //DDRAM �����������, ����� ���������
	uint8_t parm7[] = {0xC,1}; //�������� �������
	uint8_t parm8[] = {0x2,1}; //������� �����
	SendCommandToScreen((void*)parm1);
	AddTimer(5, SendCommandToScreen, false, (void*)parm1, 2);
	AddTimer(7, SendCommandToScreen, false, (void*)parm1, 2);
	AddTimer(9, SendCommandToScreen, false, (void*)parm1, 2);
	AddTimer(11, SendCommandToScreen, false, (void*)parm2, 2);
	//������� � 4 ��� �����
	AddTimer(13, SendCommandToScreen, false, (void*)parm3, 2);
	AddTimer(15, SendCommandToScreen, false, (void*)parm4, 2);
	AddTimer(17, SendCommandToScreen, false, (void*)parm5, 2);
	AddTimer(20, SendCommandToScreen, false, (void*)parm6, 2);
	AddTimer(22, SendCommandToScreen, false, (void*)parm7, 2);
	AddTimer(24, SendCommandToScreen, false, (void*)parm8, 2);
	AddTimer(26, send_init_text, false, 0, 0);
}
uint8_t SelectedShunt = 1;//!����������, ���������� �� ��������� ����
/*
 * @brief ������� ���������� �� �����
 * @details �������� ��� � ������� ������� ����������� ��������
 */
void WriteMeasurmentResultToScreen(void *p)
{
	uint32_t rshunt = 100000;
	rshunt = GetParam(10 + SelectedShunt);
	uint32_t rc = 1000000/rshunt;
	uint32_t vc = 6502000/ActualVDDA;
	uint32_t avg_res = 0;
	uint16_t temp_avg = 0;
	for(uint8_t i = 0; i < sizeof(ModeMeanVal)/2; i++)
	{
		avg_res+=ModeMeanVal[i];
	}
	avg_res/=sizeof(ModeMeanVal)/2;
	if(avg_res % sizeof(ModeMeanVal)/2 > sizeof(ModeMeanVal)/4)
	{
		avg_res++;
	}
	for(uint8_t i = 0; i < sizeof(TempSensVal)/2; i++)
	{
		temp_avg+=TempSensVal[i];
	}
	temp_avg/=sizeof(TempSensVal)/2;
	if(avg_res % sizeof(TempSensVal)/2 > sizeof(TempSensVal)/4)
	{
		temp_avg++;
	}
	uint32_t cond = vc * rc; //uS �� �������� �������
	cond = cond / avg_res;
	if( vc*rc % avg_res > avg_res/2)
	{
		cond++;
	}
	char buf[32] = {0};
	/*if (counter == 0) {
		buf[0] = 28;
		buf[1] = 1; //� �������� ������
		buf[2] = 0; //1 ������
		sprintf(&buf[3], "Conduction  a%u.%u uS  T:%u.%u", cond/10, cond - (cond/10)*10, TempSensVal[0]/10, TempSensVal[0] - (TempSensVal[0]/10)*10);
		buf[14] = 0x0A;
	}else{*/
		buf[1] = 1; //������� ������
		buf[2] = 0; //1 ������
		sprintf(&buf[3], "ae:%u.%uuS\nT:%u.%uC", (uint16_t)cond/10, (uint16_t)cond - ((uint16_t)cond/10)*10, temp_avg/10, temp_avg - (temp_avg/10)*10);
		buf[0] = FindSizeOfArray((uint8_t*)buf, 32)-3;
	WriteTextToScreen(buf);

}
