/*
 * i2c.c
 *
 *  Created on: 25 авг. 2019 г.
 *      Author: tporyvaev
 */
#include "main.h"
#include <string.h>
#include <stdlib.h>
uint8_t i2c_tx[256] = {0};
volatile bool isDMAI2CTC = true;
uint8_t shval = 0x7F;//shunt value
DMA_HandleTypeDef hdma2 = {0};
DMA_HandleTypeDef hdma3 = {0};
volatile bool I2C_Free = true;

void initI2C(void)
{ //100KHz 10ns fall/rise, ��������� � SYSCLK
	__HAL_RCC_I2C1_CLK_ENABLE();
	I2C_HandleTypeDef hi2c = {0};
	hi2c.Instance = I2C1;
	hi2c.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	//hi2c.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	//hi2c.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	//hi2c.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	hi2c.Init.Timing = 0x00C0B7FF;
	HAL_I2C_Init(&hi2c);
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
	{
	   Error_Handler();
	}
	/** Configure Digital filter
	  */
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c, 0x0f) != HAL_OK)
	{
	   Error_Handler();
	}
	//set DMA2 TX
	DMA1_Channel2->CCR &= ~(1 << 0);//Disable channel 2
	hdma2.Instance = DMA1_Channel2;
	hdma2.Init.Direction = DMA_MEMORY_TO_PERIPH;
	hdma2.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	hdma2.Init.MemInc = DMA_MINC_ENABLE;
	hdma2.Init.Mode = DMA_NORMAL;
	hdma2.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma2.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma2.Init.Priority = DMA_PRIORITY_LOW;
	HAL_DMA_Init(&hdma2);
	//set DMA3 RX
	DMA1_Channel3->CCR &= ~(1 << 0);//Disable channel 2
	hdma3.Instance = DMA1_Channel3;
	hdma3.Init.Direction = DMA_PERIPH_TO_MEMORY;
	hdma3.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	hdma3.Init.MemInc = DMA_MINC_ENABLE;
	hdma3.Init.Mode = DMA_NORMAL;
	hdma3.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma3.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma3.Init.Priority = DMA_PRIORITY_LOW;
	HAL_DMA_Init(&hdma3);
	DMA1_Channel2->CCR |= (1 << 1);//Set transfer complete interrupt
	DMA1_Channel3->CCR |= (1 << 1);//Set transfer complete interrupt
	HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 2, 0); //Set the lowest priority for DMA interput
	HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn); //Enable DMA TCIE
	I2C1->CR1 |= (1 << 14);//Set TXDMAEN
	I2C1->CR1 |= (1 << 15);//Set RXDMAEN
	I2C1->CR1 |= (1 << 7); //Set ERRIE
	I2C1->CR1 |= (1 << 6); //Set TCIE
	I2C1->CR1 |= (1 << 4); //Set NACKI
	I2C1->CR1 |= (1 << 5); //Set STOPIE
	HAL_NVIC_SetPriority(I2C1_IRQn, 3, 0);
	HAL_NVIC_EnableIRQ(I2C1_IRQn);
	I2C1->CR1 |= (1 << 0);//Set PE
}
bool isRestart = false;
uint8_t ReadSize = 0;
uint8_t *ReadDest = 0;
/**
 * @brief ���������� ������ �� �2�
 * @param p - ��������� ��������� I2CSendDef
 */
void I2CSend(void *p)
{
	isDMARXTC = false;
	isDMAI2CTC = false;
	I2CSendDef *f = (I2CSendDef *)p;
	memmove(i2c_tx, f->src, f->size);
	free(f->src);
	free(f->src);
	I2C1->CR2 &= ~0xFF00FF; //�������� SADD7:0, NBYTES
	I2C1->CR2 &= ~(1 << 10); //������� ��� �����������
	if(f->restatrt_to_read && f->read_dest != 0)
	{
		ReadSize = f->size_read;
		//config DMA3
		DMA1_Channel3->CCR &= ~(1 << 0);//Disable channel 2
		DMA1_Channel3->CNDTR &= (uint16_t)~0xFFFF; //������� �������
		DMA1_Channel3->CNDTR = (uint16_t)f->size_read;
		DMA1_Channel3->CMAR = (uint32_t)f->read_dest;
		DMA1_Channel3->CPAR = (uint32_t)&I2C1->RXDR;
		DMA1_Channel3->CCR |= (1 << 0);//Enable channel 2
		I2C1->CR2 &= ~(1 << 25); //������� ��� AUTOEND
		I2C1->CR2 |= (f->addr << 1)|(f->size << 16);//set slave addr, nbytes,
	}else{
		isRestart = false;
		I2C1->CR2 |= (f->addr << 1)|(f->size << 16)|(1 << 25);//set slave addr, nbytes, autoend
	}
	//config DMA2
	DMA1_Channel2->CCR &= ~(1 << 0);//Disable channel 2
	DMA1_Channel2->CNDTR &= (uint16_t)~0xFFFF; //������� �������
	DMA1_Channel2->CNDTR = (uint16_t)f->size;
	DMA1_Channel2->CMAR = (uint32_t)&i2c_tx[0];
	DMA1_Channel2->CPAR = (uint32_t)&I2C1->TXDR;
	DMA1_Channel2->CCR |= (1 << 0);//Enable channel 2
	I2C1->CR2 |= (1 << 13);//send start
}

//uint8_t i2c_tx_heap[256] = {0};
//uint8_t *pi2c_tx_heap = i2c_tx_heap;
/**
 * ���������, ���������� ����� ���������� � ����� ������
 * ����� ��������� ��������� ����������, �� ���, ������, �������
 * @param p - ��������� �� ��������� I2CSendDef
 * @param priority - ��������� ������ ��������
 */
void I2CSendTask(void *p, uint8_t priority)
{
	I2CSendDef *f = (I2CSendDef *)p;
	if (f->size > 0) {
		uint8_t *target = (uint8_t *)malloc(f->size);
		if (target != NULL) {
			memcpy(target, f->src, f->size);
			f->src = target;
			Task t = {I2CSend, (void *)f, priority, false, 1, (void *)&I2C_Free, sizeof(I2CSendDef)}; //������ ���� �� �������� ������
			CreateTask(&t);
		}
	}
}

volatile bool isDMARXTC = false;
void I2CRead(void *p)
{
	isDMARXTC = false;
	isDMAI2CTC = false;
	I2CSendDef *f = (I2CSendDef *)p;
	I2C1->CR2 &= ~0xFF00FF; //�������� SADD7:0, NBYTES
	I2C1->CR2 |= (f->addr << 1)|(f->size << 16)|(1 << 25)|(1 << 10);//set slave addr, nbytes, autoend
	//config DMA3
	DMA1_Channel3->CCR &= ~(1 << 0);//Disable channel 2
	DMA1_Channel3->CNDTR &= (uint16_t)~0xFFFF; //������� �������
	DMA1_Channel3->CNDTR = (uint16_t)f->size;
	DMA1_Channel3->CMAR = (uint32_t)f->src;
	DMA1_Channel3->CPAR = (uint32_t)&I2C1->RXDR;
	DMA1_Channel3->CCR |= (1 << 0);//Enable channel 2
	I2C1->CR2 |= (1 << 13);//send start
}
/**
 * TFS
 * ������ ������ EEPROM: 256 ����
 * ����� 32 ��������
 *	 0-1 �������� - ������������� ���������
 *	 2 �������� - �������� � ����������� �� ��������� ���������� ���� � ������
 *	 ������ ��������� �������� 5�� ������ � ��������� �� 4 ��������
 *	 8 ����������
 *	 ������������ ��� ������ ���������� ��������, ������� ��� �� ���� ������������, ���� ��
 *	 ���������� ������ ���� �� �������
 *	 �������� ���������� ������������ ���������� ��������� � 0x0, �� �������, � ������ ������������� �������
 *	 ������ ���������� � �������� ����� ���������+1
 *	 ����������� ������ ���������� ������������ ��������� CRC-16 MODBUS (�.�. ��� ���� �������)
 *	 ������ � ������ ������������� ��������� ��������� �� <= 8 ���� �� ���, ����� �� �������� ����� �����
 *
volatile uint8_t current_pointer_table[8] = {0};
uint8_t PointerTable[8] = {0};
volatile uint8_t cpt_page = 0;
*/
/*
/** * @brief �������� �������� �� ����� � ������������ ���

void LoadPointerTable(void *p)
{
	uint64_t pointer = 0;
	memcpy(&pointer,(void*)current_pointer_table,8);
	for(uint8_t i = 0; i < 8; i++)
	{
		PointerTable[i] |= (pointer >> i*5)  & 0x1f;
	}
}
void UnloadPointerTable(void *p)
{
	uint64_t pointer = 0;
	for(uint8_t i = 0; i < 8; i++)
	{
		pointer |= PointerTable[i];
	}
} */
/*/**
 * @brief ��������� ������� ����������
void ReadPointerTable()
{
	uint8_t adr[2] = {0};
	AddTimer(10, LoadPointerTable,0,0,0); //��������� �������
	I2CSendDef t = {(void *)adr, EEPROM_Addrees_Size, EEPROM_Address, 1, EEPROM_Pointer_TableSize, (void *)current_pointer_table};
	I2CSendTask(&t, 3);
}*/
uint8_t *eeprom_src = 0;
uint16_t eeprom_src_crc = 0;
uint8_t eeprom_src_pointer = 0; //��������� �� ��� �� ������������
uint8_t eeprom_src_addr = 0; //��������� �� "������" �����
uint8_t eeprom_src_size = 0;
bool isPageLast = false;
volatile uint8_t eeprom_pagewrite_buffer[EEPROM_Page_Size + EEPROM_Addrees_Size] = {0};
volatile uint8_t eeprom_read_buffer[8] = {0};
/*
 * @brief ���������� ��������� ������ � EEPROM
 * src ������ ���� ����������, �� �� �����������
 */
void EEPROMWrite(uint8_t *src, uint8_t size, uint8_t address)
{
	I2CSendDef t1  = {0};
	if (size > EEPROM_Page_Size) {
		memcpy((void *)&eeprom_pagewrite_buffer[EEPROM_Addrees_Size], src, EEPROM_Page_Size);
		memcpy((void *)&eeprom_pagewrite_buffer[0], &address, EEPROM_Addrees_Size);
		eeprom_src_pointer += EEPROM_Page_Size;
		eeprom_src_addr = address+EEPROM_Page_Size;
		eeprom_src_size = EEPROM_Page_Size;
		eeprom_src = src+EEPROM_Page_Size;
		eeprom_src_crc = ModRTU_CRC((uint8_t*)&eeprom_pagewrite_buffer[EEPROM_Addrees_Size], EEPROM_Page_Size);
		t1 = (I2CSendDef){(void *)eeprom_pagewrite_buffer, EEPROM_Page_Size + EEPROM_Addrees_Size, EEPROM_Address};
		/*Task t = {internal_SendNextPage, 0, 3, 0,1, (void*)&isPageWrote,0};
		CreateTask(&t);*/
	}else{
		memcpy((void *)&eeprom_pagewrite_buffer[EEPROM_Addrees_Size], src, size);
		memcpy((void *)&eeprom_pagewrite_buffer[0], &address, EEPROM_Addrees_Size);
		eeprom_src_crc = ModRTU_CRC((uint8_t*)&eeprom_pagewrite_buffer[EEPROM_Addrees_Size], size);
		eeprom_src_size = size;
		t1 = (I2CSendDef){(void *)eeprom_pagewrite_buffer, EEPROM_Addrees_Size + size, EEPROM_Address};
		isPageLast = true;
	}
	AddTimer(10, internal_ReadLastSendToEEPROM, 0,0,0);
	I2CSendTask(&t1, 3);
}
void internal_SendNextPage(void *p)
{
	isSentReading = false;
	uint16_t CurrentCRC = ModRTU_CRC((uint8_t*)eeprom_read_buffer, eeprom_src_size);
	if (CurrentCRC == eeprom_src_crc) {
		if (!isPageLast) {
			uint8_t remaning_bts = eeprom_src_size - eeprom_src_pointer;
			I2CSendDef t1  = {0};
			if(remaning_bts > EEPROM_Page_Size)
			{
				memcpy((void *)&eeprom_pagewrite_buffer[EEPROM_Addrees_Size], eeprom_src, EEPROM_Page_Size);
				memcpy((void *)&eeprom_pagewrite_buffer[0], &eeprom_src_addr, EEPROM_Addrees_Size);
				eeprom_src_pointer += EEPROM_Page_Size;
				eeprom_src_addr +=EEPROM_Page_Size;
				eeprom_src_size = EEPROM_Page_Size;
				eeprom_src +=EEPROM_Page_Size;
				eeprom_src_crc = ModRTU_CRC((uint8_t*)&eeprom_pagewrite_buffer[EEPROM_Addrees_Size], EEPROM_Page_Size);
				t1 = (I2CSendDef){(void *)eeprom_pagewrite_buffer, EEPROM_Page_Size + EEPROM_Addrees_Size, EEPROM_Address};
				/*Task t = {internal_SendNextPage, 0, 3, 0,1, (void*)&isPageWrote,0};
				CreateTask(&t);*/
			}else{
				memcpy((void *)&eeprom_pagewrite_buffer[EEPROM_Addrees_Size], eeprom_src, remaning_bts);
				memcpy((void *)&eeprom_pagewrite_buffer[0], &eeprom_src_addr, EEPROM_Addrees_Size);
				eeprom_src_crc = ModRTU_CRC((uint8_t*)&eeprom_pagewrite_buffer[EEPROM_Addrees_Size], remaning_bts);
				eeprom_src_size = remaning_bts;
				t1 = (I2CSendDef){(void *)eeprom_pagewrite_buffer, EEPROM_Addrees_Size + remaning_bts, EEPROM_Address};
				isPageLast = true;
			}
			AddTimer(10, internal_ReadLastSendToEEPROM, 0,0,0);
			I2CSendTask(&t1, 3);
		}
	}
}
volatile bool isSentReading = false;
volatile bool isSentRead = false;
void internal_ReadLastSendToEEPROM(void *p)
{
	isSentReading = true;
	isSentRead = false;
	uint8_t addr = 0;
	if(!isPageLast) //���� �������� �� ���� ���������
	{
		addr = eeprom_src_addr-EEPROM_Page_Size; //������ ��� ���������� �� 8 ���� ������
	}else{
		addr = eeprom_src_addr; //���� �������� ���������, �� src ��������� ����� �� ���
	}
	I2CSendDef t1 = { &addr, EEPROM_Addrees_Size, EEPROM_Address, 1, eeprom_src_size, (uint8_t*)eeprom_read_buffer};
	I2CSendTask(&t1, 3);
	Task t = {internal_SendNextPage, 0, 3, 0,1, (void*)&isSentRead,0};
	CreateTask(&t);
}
void internal_Send2Page()
{
	uint8_t buffer2[] = {0x8, 0x0, 0x0,0x0,0x0, 0xf4,0x1, 0xa, 0x0};
	I2CSendDef t1 = {(void *)buffer2, 9, EEPROM_Address};
	I2CSendTask(&t1, 3);
}
/*
 * @brief ���������� ����������� �������
 * @details 4 24 ������ ����� ��� ������������� �����, 1 16 ������ ������ ��� ������ � 1 8 ������ ��� ������� ����������
 * ���������� - 100���, 47���, 10��� ����, 500 �� �����, 10 ��/���� ������
 * ��������: ������ � EEPROM ������� �����������: ������ �������� - 8 ����, ���������� � �������� 8 �����
 * 0-7
 * 8-15
 * � �.�. ������ ����� �������� ������ ������� �� �����������, ���� n > 8 �� ������ ����, ����� ������ ��������
 * ������� ������ ������ � ������ ��� �� ����� ��������
 * http://www.hobbytronics.co.uk/image/data/tutorial/arduino-24lc256-eeprom/page_write.jpg
 * ������ ���� ������� ���� 0x1 0x86 0xa0 ��� 0xa0, 0x86, 0x1
 * @param p ��� ��������������
 */
void WriteProfile(void *p)
{
	uint8_t buffer1[] = {0x0, 0xA0,0x86,0x1, 0x50,0xC3,0x00, 0x50,0x28};
	I2CSendDef t = {(void *)buffer1, 9, EEPROM_Address};
	I2CSendTask(&t, 3);
	AddTimer(15, internal_Send2Page, 0, 0, 0);
}

volatile uint8_t Profile[15] = {0};
volatile bool isProfileReading = false;
volatile bool isProfileRead = false;

uint32_t resistance1 = 0;
uint32_t resistance2 = 0;
uint32_t resistance3 = 0;
uint32_t resistance4 = 0;
uint16_t intercept_temp = 0;
uint8_t slope_temp = 0;
void PrepareProfile(void *p)
{
	memcpy(&resistance1, (void *)&Profile[0], 3); //���������� 1 �������������
	memcpy(&resistance2, (void *)&Profile[3], 3); //2
	memcpy(&resistance3, (void *)&Profile[6], 3); //3
	memcpy(&resistance4, (void *)&Profile[9], 3);//4
	memcpy(&intercept_temp, (void *)&Profile[12], 2); //����� �����������
	memcpy(&slope_temp, (void *)&Profile[14], 1); //������ �����������
}
/*
 * @brief ������ ������� � ���������� � ������ Profile
 */
void ReadProfile(void *p)
{
	uint8_t adr[2] = {0};
	//adr[0] = 0x0;
	isProfileReading = true;
	Task ta = {PrepareProfile,0,3,0,1,(void *)&isProfileRead,0};
	CreateTask(&ta);
	I2CSendDef t = {(void *)adr, EEPROM_Addrees_Size, EEPROM_Address, 1, 15, (void *)Profile};
	I2CSendTask(&t, 3);
}
/*
 * @brief ���������� ��������� �������� � target
 * @details ������ target �� ����������! ��������� ��� ����� ��������������
 * @param number 11,12,13 - 1,2,3,4 �������������; 2 - �����; 3 - ������
 */
uint32_t GetParam(uint8_t number)
{
	switch(number)
	{
	case 11:
		return resistance1;
		break;
	case 12:
		return resistance2;
		break;
	case 13:
		return resistance3;
		break;
	case 14:
		return resistance4;
		break;
	case 2:
		return intercept_temp;
		break;
	case 3:
		return slope_temp;
		break;
	default:
		return 0;
	}
}
//���������� ������
/*
 * 	P0 - D4
	P1 - D5
	P2 - D6
	P3 - D7
	P6 - RS
	P7 - E
 */
/**
 * @brief ���������� ������ �������� �� �����
 * @param code 0,1,2,3 - D4 D5 D6 D7, 4,5 - LED
 * @param RS - RS
 * @param mode - 1 - 4 ���
 *  */
void WriteByteToScreen(uint8_t code, uint8_t RS, uint8_t mode)
{
	uint8_t high_buf = 0;
	uint8_t low_buf = 0;
	if(RS)
	{
		ScreenRegister |= (1 << 6);
	}else{
		ScreenRegister &= ~(1 << 6);
	}
	ScreenRegister &= ~(1 << 7);
	ScreenRegister &= ~0xF; //������
	ScreenRegister |= code >> 4; //���������� ������� ����
	high_buf |= ScreenRegister;
	if (mode) {
		ScreenRegister &= ~0xF; //������ ��� ���
		ScreenRegister |= code & 0xF; //������ �������
		low_buf |= ScreenRegister;
	}
	if(mode)
	{
		uint8_t send[] = { 0x01, high_buf | (1 << 7), high_buf, low_buf | (1 << 7), low_buf}; //�������� enable �� 5 �� SD, ������ ���
		I2CSendDef f = { send, 5, SCREEN_Address};
		I2CSendTask((void *)&f, 3);
	}else{
		uint8_t send[] = { 0x01, high_buf | (1 << 7), high_buf}; //�������� enable �� 5 �� SD, ������ ���
		I2CSendDef f = { send, 3, SCREEN_Address};
		I2CSendTask((void *)&f, 3);
	}
}
uint8_t *text_buff = {0};
uint8_t ptext_buff = 0;
uint8_t text_trans_size = 0;
volatile bool NextByteTextTransaction = false;
volatile bool isTextTransaction = false;
bool internal_isTransaction = false;
/**
 *@brief ���������� ����� �� �����
 *@param text - ��������� �� ������ � �������. ��������: ������ ���� - ������ ������������� ������, 2 - ���� ������� �� �����
 *@param 3 ���� - ����� ������
 *@param  �� ����� 32 ����,
 *@param ���� 0x0A - ������� ������
 */
void WriteTextToScreen(void *ptext)
{
	if (!isTextTransaction && !internal_isTransaction) { //��������� �� ���������� �� �������� � ������ ������
		uint8_t *text = (uint8_t *)ptext;
		uint8_t size = *text;
		text++;
		uint8_t isscreenclean = *text;
		text++;
		uint8_t line = *text;
		text++;
		text_trans_size = size;
		internal_isTransaction = true;
		//isTextTransaction = true;
		ptext_buff = 0;
		if (size > 0) {
			text_buff = (uint8_t *)malloc(size);
			if (text_buff != NULL) {
				memcpy(text_buff, text, size);
				//internal_text_send(0);
				if (isscreenclean) {
					uint8_t param1[] = {0x1, 1};//������ �������
					SendCommandToScreen((void *)param1);
				}
				uint8_t param2[] = {0x80, 1};//���������� ������� �� ������ ������
				if (line) {
					param2[0] = 0xC0;//���� �����, ��� ����� �� ������
				}
				AddTimer(10, SendCommandToScreen, 0,(void *)param2,2);
				AddTimer(15, internal_text_send, 0,0,0);
				//Task t = {internal_text_send, 0, 3, 0, 1, &NextByteTextTransaction, 0};
				//CreateTask(&t); //���� ���� ��� �������� ����� ������
				}
			}
		}
}
void internal_text_send(void *p)
{
	if(ptext_buff == 0)
	{
		isTextTransaction = true;
	}
	WriteByteToScreen(*(text_buff + ptext_buff), 1, 1);
	if(ptext_buff < text_trans_size-1)
	{
		ptext_buff++;
		if (*(text_buff + ptext_buff) != 0x0A) {//��������� �� ������� �������
			Task t = {internal_text_send, 0, 3, 0, 1, (void*)&NextByteTextTransaction, 0};
			CreateTask(&t);
		}else{
			uint8_t param1[] = {0xc0, 1};//������� ������ �� ������ ������
			Task t1 = {SendCommandToScreen, (void*)param1, 3,0,1, (void*)&NextByteTextTransaction, 2};
			CreateTask(&t1); //���� ���� ������� NextByteTextTransaction, �� �� ���������� �������� ����� �������� NextByteTextTransaction
			Task t = {internal_text_send, 0, 3, 0, 1, (void*)&NextByteTextTransaction, 0};
			CreateTask(&t); //���� ���� ��� �������� ����� ������
			ptext_buff++;
		}
	}else{
		ptext_buff = 0;
		isTextTransaction = false;
		internal_isTransaction = false;
		free(text_buff);
		free(text_buff);
	}
}
/**
 * @brief ���������� ������� � �������
 * @param command �������
 * @param mode - 1 - 4 ����, 0 - 8 ���(� ������ 8 ��� ������� ���� ������ 0)
 */
void SendCommandToScreen(void *param)
{
	uint8_t *par = (uint8_t *)param;
	WriteByteToScreen(*par ,0, *(par+1));
}
/**
 * @brief ���������� ����� � �������
 * @param param �����
 */
void SendCharToScreen(void *param)
{
	uint8_t *par = (uint8_t *)param;
	WriteByteToScreen(*par,1,1);
}
