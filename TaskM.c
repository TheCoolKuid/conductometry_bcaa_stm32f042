/*
 * TaskM.c
 *
 *  Created on: Aug 9, 2019
 *      Author: tporyvaev
 */
#include <stdbool.h>
#include <stdlib.h>
#include "Main.h"
#include "stm32f0xx_it.h"
#include <string.h>
Task Tasklist[MaxTasksNumber] = {0};
Task *inTaskP = Tasklist;

uint8_t heap[MaxTasksNumber*MaxTaskHeapSize+1] = {0};
uint8_t *pheap = heap;
/**
 * @brief Adds new task to tasks list
 * @param Pointer of task var
 * @retval 0-(MaxTasksNumber-1) - id of task, MaxTasksNumber - no empty task cells
 */
uint8_t CreateTask(Task *t)
{
	for(uint8_t i = 0; i < MaxTasksNumber; i++)
	{
		if((inTaskP+i)->Function == 0)
		{
			if(t->psize < MaxTaskHeapSize)
			{
				(inTaskP+i)->Condition = t->Condition;
				(inTaskP+i)->Function = t->Function;
				(inTaskP+i)->Priority = (t->Priority)+1;
				(inTaskP+i)->Repetition = t->Repetition;
				(inTaskP+i)->isSuspend = t->isSuspend;
				(inTaskP+i)->param = t->param;
				(inTaskP+i)->psize = t->psize;
				memcpy((pheap + i*MaxTaskHeapSize), t->param, t->psize);
				(inTaskP+i)->pheap = (pheap + i*MaxTaskHeapSize);
				return i;
			}
			return MaxTasksNumber;
		}
	}
	MainStatusRegister |= (1 << 2);//��������� �� ��������� ��������� ������
	return MaxTasksNumber;
}
/**
 * @brief Deletes task from tasks list
 * @param id of task
 *
 */
void DeleteTask(uint8_t id)
{
	if(id < MaxTasksNumber)
	{
		(inTaskP+id)->Function = 0;
	}
}
/**
 * @brief Creates list of task in priority order
 */
uint8_t idList[MaxTasksNumber] = {0};
void CreateTaskList()
{
	uint8_t *idlp = idList;
	for(uint8_t i = 1; i < 5; i++)
	{
		for(uint8_t ii = 0; ii < MaxTasksNumber; ii++)
		{
			if((inTaskP + ii)->Priority == i && (inTaskP + ii)->Function != 0)
			{
				*idlp = ii;
				if(idlp == &idList[MaxTasksNumber-1])
				{
					idlp = &idList[0];
				}else{
					idlp++;
				}
			}
		}
	}
}

void TaskManager()
{
	CreateTaskList(); //����������� �����
	for(;;)
	{
		for(uint8_t i = 0; i < MaxTasksNumber; i++)
		{
			if(*((bool *)(inTaskP+idList[i])->Condition) &&
					!((inTaskP+idList[i])->isSuspend)
					&& (inTaskP+idList[i])->Repetition > 0
					&& (inTaskP+idList[i])->Function != 0)
			{
				(inTaskP+idList[i])->Function((void *)(inTaskP+idList[i])->pheap);
				if((inTaskP+idList[i])->Repetition != 255)
				{
					(inTaskP+idList[i])->Repetition--;
					if((inTaskP+idList[i])->Repetition == 0)
					{
						DeleteTask(idList[i]);
					}
				}
				*((bool *)(inTaskP+idList[i])->Condition) = false;
			}
		}
		//RegisterWatch(0); //��������� ��������
		TimerManager();//��������� �������
		CreateTaskList();
	}
}
//Timer TimerList[MaxTimersNumber] = {0};
Timer *timert[MaxTimersNumber] = {0};
//uint8_t timer_heap[MaxTimersNumber*MaxTimersHeapSize+1] = {0};
/**
 * @brief ������� ������. ��������: �������, ������� ����� ��������� ������ ����� ����� ���������� ���������
 * @param delay �������� � ��
 * @param target �������, ������� ������ ���� ���������.
 * @param repeat ����������� �� ������
 * @param arg �������� �������
 * @param arg_size ������ ��������� � ������, �� ������ 8!
 * @retval ���������� id ������� ��� MaxTimersNumber � ������ ��������� �����
 */
uint8_t AddTimer(uint32_t delay, void (*target)(void*), bool repeat, void* arg, uint8_t arg_size)
{
	for(uint8_t i = 0; i < MaxTimersNumber; i++)
	{
		if(timert[i] == 0)
		{
			Timer t = {delay, target, repeat, 0,0};
			memcpy(&t.last_millis ,(void *)&Tick, 4);
			if (arg_size > 0) {
				uint8_t *target = (uint8_t *)malloc(arg_size);
				if(target != NULL)
				{
					memcpy(target, arg, arg_size);
					t.heap = target;
				}else{
					while(1);//������
				}
			}
			Timer *targ = (Timer *)malloc(sizeof(Timer));
			if (targ != NULL) {
				memcpy(targ, &t, sizeof(Timer));
				timert[i] = targ;
				return i;
			}
			return MaxTimersNumber;
		}
	}
	MainStatusRegister |= (1 << 5);//�������� �� ������
	return MaxTimersNumber;
}

/**
 * @brief ������� ������
 * @param id id �������
 * @retval ���������� id � ������ ������, -1 � ������, ���� id > MaxTimersNumber
 */
int DeleteTimer(uint8_t id)
{
	if(id < MaxTimersNumber)
	{
		free(timert[id]);
		free(timert[id]);
		timert[id] = 0;
		return id;
	}
	return -1;
}

void TimerManager()
{
	for(uint8_t i = 0; i < MaxTimersNumber; i++)
	{
		if(timert[i]->last_millis + timert[i]->delay <= Tick && timert[i]->target != 0)
		{
			timert[i]->target((void*)timert[i]->heap);//��������� �������
			if(timert[i]->repeat)
			{
				memcpy(&timert[i]->last_millis ,(void *)&Tick, 4); //���������� ������� �������� �������
			}else{
				free(timert[i]->heap);
				free(timert[i]->heap);
				if(DeleteTimer(i) == -1) //���� ������ �� ���������, �������
				{
					MainStatusRegister |= (1 << 5);//����� ������
				}
			}
		}
	}
}
